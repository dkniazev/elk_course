package com.epam.elk;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * @author Dmitrii_Kniazev
 * @since 01/18/2017
 */
public class DirImporter {
    private static final Logger LOG = LoggerFactory.getLogger(DirImporter.class);

    private static final int DEFAULT_FILES_NUM = 15;

    public static void main(String[] args) throws IOException, JAXBException {
        if (args.length < 1) {
            LOG.error("Current number of parameters" + args.length);
            LOG.error("Usage: elasticimporter <dir> <max>");
            System.exit(1);
        }

        final Path dirPath = Paths.get(args[0]);
        if (!Files.isDirectory(dirPath)) {
            LOG.error("Path " + dirPath + " is not directory");
            System.exit(1);
        }

        final int limit = args.length > 1 ? Integer.parseInt(args[1]) : DEFAULT_FILES_NUM;

        final Stream<File> files = Files.walk(dirPath)
                .filter(Files::isRegularFile)
                .map(Path::toFile)
                .filter(f -> f.getName().endsWith(".xml"))
                .limit(limit);

        final SingleFileImporter singleFileImporter = new SingleFileImporter();
        files.forEach(f -> {
            try {
                singleFileImporter.importFile(f);
            } catch (JAXBException | JsonProcessingException e) {
                LOG.error("Failed file import:" + f.getName());
                e.printStackTrace();
            }
        });
    }
}
