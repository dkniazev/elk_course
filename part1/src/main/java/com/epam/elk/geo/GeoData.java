package com.epam.elk.geo;

/**
 * @author Dmitrii_Kniazev
 * @since 01/18/2017
 */
public class GeoData {
    private String geonameid;
    private String name;
    private String asciiname;
    private String alternatenames;
    private double latitude;
    private double longitude;
    private String featureClass;
    private String featureCode;
    private String countryCode;
    private String cc2;
    private String admin1Code;
    private String admin2Code;
    private String admin3Code;
    private String admin4Code;
    private long population;
    private int elevation;
    private int dem;
    private String timezone;
    private String modificationDate;

    public static GeoData parseString(final String src) {
        final String[] split = src.split("\t");
        final GeoData geoData = new GeoData();
        geoData.setGeonameid(split[0]);
        geoData.setName(split[1]);
        geoData.setAsciiname(split[2]);
        geoData.setAlternatenames(split[3]);
        geoData.setLatitude(Double.parseDouble(split[4]));
        geoData.setLongitude(Double.parseDouble(split[5]));
        geoData.setFeatureClass(split[6]);
        geoData.setFeatureCode(split[7]);
        geoData.setCountryCode(split[8]);
        geoData.setCc2(split[9]);
        geoData.setAdmin1Code(split[10]);
        geoData.setAdmin2Code(split[11]);
        geoData.setAdmin3Code(split[12]);
        geoData.setAdmin4Code(split[13]);
        geoData.setPopulation(Integer.parseInt(split[14]));
        geoData.setElevation(split[15].equals("") ? 0 : Integer.parseInt(split[15]));
        geoData.setDem(Integer.parseInt(split[16]));
        geoData.setTimezone(split[17]);
        geoData.setModificationDate(split[18]);
        return geoData;
    }

    public String getGeonameid() {
        return geonameid;
    }

    public void setGeonameid(String geonameid) {
        this.geonameid = geonameid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAsciiname() {
        return asciiname;
    }

    public void setAsciiname(String asciiname) {
        this.asciiname = asciiname;
    }

    public String getAlternatenames() {
        return alternatenames;
    }

    public void setAlternatenames(String alternatenames) {
        this.alternatenames = alternatenames;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getLocation() {
        return latitude + "," + longitude;
    }

    public String getFeatureClass() {
        return featureClass;
    }

    public void setFeatureClass(String featureClass) {
        this.featureClass = featureClass;
    }

    public String getFeatureCode() {
        return featureCode;
    }

    public void setFeatureCode(String featureCode) {
        this.featureCode = featureCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCc2() {
        return cc2;
    }

    public void setCc2(String cc2) {
        this.cc2 = cc2;
    }

    public String getAdmin1Code() {
        return admin1Code;
    }

    public void setAdmin1Code(String admin1Code) {
        this.admin1Code = admin1Code;
    }

    public String getAdmin2Code() {
        return admin2Code;
    }

    public void setAdmin2Code(String admin2Code) {
        this.admin2Code = admin2Code;
    }

    public String getAdmin3Code() {
        return admin3Code;
    }

    public void setAdmin3Code(String admin3Code) {
        this.admin3Code = admin3Code;
    }

    public String getAdmin4Code() {
        return admin4Code;
    }

    public void setAdmin4Code(String admin4Code) {
        this.admin4Code = admin4Code;
    }

    public long getPopulation() {
        return population;
    }

    public void setPopulation(long population) {
        this.population = population;
    }

    public int getElevation() {
        return elevation;
    }

    public void setElevation(int elevation) {
        this.elevation = elevation;
    }

    public int getDem() {
        return dem;
    }

    public void setDem(int dem) {
        this.dem = dem;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(String modificationDate) {
        this.modificationDate = modificationDate;
    }
}
