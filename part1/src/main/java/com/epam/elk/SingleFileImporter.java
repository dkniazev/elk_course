package com.epam.elk;

import com.epam.elk.geo.GeoData;
import com.epam.elk.xml.ProductRecord;
import com.epam.elk.xml.XmlParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.action.support.WriteRequest.RefreshPolicy;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.BulkIndexByScrollResponse;
import org.elasticsearch.index.reindex.DeleteByQueryAction;
import org.elasticsearch.index.reindex.DeleteByQueryRequestBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * @author Dmitrii_Kniazev
 * @since 01/18/2017
 */
public class SingleFileImporter extends ElasticConnector {
    private static final Logger LOG = LoggerFactory.getLogger(SingleFileImporter.class);

    private static final String INDEX = "products";
    private static final String TYPE = "goods";

    private static final String GEO_INDEX = "geonames";
    private static final String GEO_TYPE = "countries";
    private static final int MAX_GEO_DATA = 3000000;

    private final XmlParser xmlParser;
    private static final ObjectMapper JSON_MAPPER = new ObjectMapper();

    public SingleFileImporter() throws UnknownHostException, JAXBException {
        super();
        xmlParser = new XmlParser();
    }

    public int importGeodataFile(final File file) throws IOException {
        final Stream<GeoData> lines = Files.lines(Paths.get(file.toURI())).map(GeoData::parseString);

        LOG.debug("File:" + file.getName());

        final Iterator<GeoData> iterator = lines.iterator();

        int counter = 0;
        int totalCounter = 0;
        BulkRequestBuilder bulk = getClient().prepareBulk();
        while (iterator.hasNext()) {
            counter += 1;
            final GeoData nextData = iterator.next();
            try {
                final byte[] source = JSON_MAPPER.writeValueAsBytes(nextData);
                final IndexRequestBuilder request = getClient()
                        .prepareIndex(GEO_INDEX, GEO_TYPE, nextData.getGeonameid())
                        .setSource(source);
                bulk.add(request);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            if (counter >= 5000) {
                final BulkResponse responses = bulk.get();
                if (responses.hasFailures()) {
                    LOG.error("Batch with failure");
                    responses.forEach(r -> {
                        if (r.isFailed()) LOG.error("Failed id:" + r.getId());
                    });
                }
                bulk = getClient().prepareBulk();
                totalCounter += counter;
                LOG.debug("Records processed:" + responses.getItems().length + "; Total:" + totalCounter);
                counter = 0;
            }
            if (totalCounter > MAX_GEO_DATA) {
                break;
            }
        }
        if (counter > 0) {
            final BulkResponse responses = bulk.get();
            if (responses.hasFailures()) {
                LOG.error("Batch with failure");
                responses.forEach(r -> {
                    if (r.isFailed()) LOG.error("Failed id:" + r.getId());
                });
            }
            LOG.debug("Records processed:" + responses.getItems().length + "; Total:" + totalCounter);
            totalCounter += counter;
        }
        LOG.debug("Total records processed:" + totalCounter);
        return totalCounter;
    }

    public int importFile(final File file)
            throws JAXBException, JsonProcessingException {
        return importFile(file, RefreshPolicy.NONE);
    }

    public int importFile(final File file, RefreshPolicy refreshPolicy)
            throws JAXBException, JsonProcessingException {
        final Collection<ProductRecord> documents = xmlParser.getDocuments(file);
        final BulkRequestBuilder bulk = getClient().prepareBulk().setRefreshPolicy(refreshPolicy);
        documents.stream().map(doc -> {
            final String id = doc.getSku();
            try {
                final byte[] source = JSON_MAPPER.writeValueAsBytes(doc.toMap());
                return getClient().prepareIndex(INDEX, TYPE, id).setSource(source);
            } catch (JsonProcessingException e) {
                LOG.error("Parse to JSON doc with sku: " + doc.getSku());
                e.printStackTrace();
            }
            return null;
        }).filter(Objects::nonNull).forEach(bulk::add);

        final BulkResponse responses = bulk.get();
        if (responses.hasFailures()) {
            LOG.error(responses.buildFailureMessage());
        }
        LOG.debug("File: " + file.getName() + "; Records processed:" + responses.getItems().length);
        return responses.getItems().length;
    }

    public DeleteByQueryRequestBuilder getDeleteBuilder() {
        return DeleteByQueryAction.INSTANCE.newRequestBuilder(getClient());
    }

    public static void main(String[] args) throws IOException, JAXBException, InterruptedException {
        if (args.length != 1) {
            LOG.error("Current number of parameters" + args.length);
            LOG.error("Usage: elasticimporter <path>");
            System.exit(1);
        }

        final File filePath = new File(args[0]);
        final SingleFileImporter fileImporter = new SingleFileImporter();
        fileImporter.importFile(filePath, RefreshPolicy.WAIT_UNTIL);

        final DeleteByQueryRequestBuilder deleteRequest = fileImporter.getDeleteBuilder()
                .filter(QueryBuilders.matchQuery("manufacturer", "Samsung"))
                .source(INDEX);
        final BulkIndexByScrollResponse response = deleteRequest.get();
        final long deleted = response.getDeleted();
        LOG.debug("Deleted records:" + deleted);
    }
}
