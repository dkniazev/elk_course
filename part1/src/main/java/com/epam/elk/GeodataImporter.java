package com.epam.elk;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;

/**
 * @author Dmitrii_Kniazev
 * @since 01/18/2017
 */
public class GeodataImporter {
    private static final Logger LOG = LoggerFactory.getLogger(GeodataImporter.class);
    public static void main(String[] args) throws JAXBException, IOException {
        if (args.length < 1) {
            LOG.error("Current number of parameters " + args.length);
            LOG.error("Usage: elasticimporter <path> ");
            System.exit(1);
        }
        final File filePath = new File(args[0]);
        final SingleFileImporter fileImporter = new SingleFileImporter();
        fileImporter.importGeodataFile(filePath);
    }
}
