package com.epam.elk.xml;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.sun.xml.bind.v2.runtime.JAXBContextImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.Collection;

/**
 * @author Dmitrii_Kniazev
 * @since 01/16/2017
 */
public final class XmlParser {
    private static final Logger LOG = LoggerFactory.getLogger(XmlParser.class);

    private final Unmarshaller unmarshaller;

    public XmlParser() throws JAXBException {
        final JAXBContext jc = JAXBContextImpl.newInstance(Products.class);
        unmarshaller = jc.createUnmarshaller();
        LOG.debug("Unmarshaller created");
    }

    public Collection<ProductRecord> getDocuments(final File file) throws JAXBException, JsonProcessingException {
        final Products products = (Products) unmarshaller.unmarshal(file);
        LOG.debug(String.format("File:%s; Documents:%d", file.getName(), products.getProducts().size()));
        return products.getProducts();
    }
}
