package com.epam.elk.xml;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;

/**
 * @author Dmitrii_Kniazev
 * @since 01/16/2017
 */
@XmlRootElement(name = "products")
final class Products {
    @XmlElement(name = "product")
    private Collection<ProductRecord> products = null;

    Collection<ProductRecord> getProducts() {
        return products;
    }
}
