package com.epam.elk;

import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.UnknownHostException;


/**
 * @author Dmitrii_Kniazev
 * @since 01/16/2017
 */
public class ElasticConnector {
    private static final Logger LOG = LoggerFactory.getLogger(ElasticConnector.class);

    private static final String ES_HOST = "192.168.99.100";
    private static final String ES_CLUSTER_NAME = "elasticsearch";

    private final TransportClient client;

    public ElasticConnector() throws UnknownHostException {
        this(ES_HOST, ES_CLUSTER_NAME);
    }

    public ElasticConnector(String host, String clusterName) throws UnknownHostException {
        final Settings esSettings = Settings.builder()
                .put("cluster.name", clusterName).build();

        final InetSocketTransportAddress esHost = new InetSocketTransportAddress(
                InetAddress.getByName(host), 9300);

        this.client = new PreBuiltTransportClient(esSettings)
                .addTransportAddress(esHost);
    }

    protected TransportClient getClient() {
        return client;
    }
}
