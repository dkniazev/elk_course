package com.epam.elk;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * @author Dmitrii_Kniazev
 * @since 01/16/2017
 */
public class ElasticImporterTest {
    @Test
    public void testSomeLibraryMethod() {
        assertTrue("someLibraryMethod should return 'true'", true);
    }
}
