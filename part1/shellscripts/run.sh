#!/usr/bin/env bash

#sysctl -w vm.max_map_count=262144 on host machine
#network.host: The node will bind to this hostname or IP address
# and publish (advertise) this host to other nodes in the cluster. (for using 9300 port)
docker run --name es -p 9200:9200 -p 9300:9300 elasticsearch:alpine -Enetwork.host=_eth0_
docker run --name kibana --link es:elasticsearch -p 5601:5601 -d kibana
